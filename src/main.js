import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase'


  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAX3HeX1ofNLcz_M7N_0YbUehOiGhC71-0",
    authDomain: "fir-test-dc432.firebaseapp.com",
    databaseURL: "https://fir-test-dc432.firebaseio.com",
    projectId: "fir-test-dc432",
    storageBucket: "fir-test-dc432.appspot.com",
    messagingSenderId: "1093513331774"
  };
  firebase.initializeApp(config);

  window.firebase = firebase;

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
